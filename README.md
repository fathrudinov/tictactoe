### Tictactoe

This small project was originally implemented as a part of [Cops'N'Robber](https://gitlab.com/fathrudinov/cops-n-robber) 
and then was separeted to its own repository. 

The project includes GUI, perfect player implementation and also strategy hints.

The engine in the project is implemented using the direct solution
from Newell and Simon's 1972 tic-tac-toe program.
For more information about the strategy used, see [wiki](https://en.wikipedia.org/wiki/Tic-tac-toe#Strategy).

### Manual game

To play tic-tac-toe against a bot or even in multiplayer mode, 
type in console in projects directory:

```bash
$ python -m tictactoe
```
