"""Creates the game's board and run its main loop."""
from .gui import TicTacToeBoard

"""from networkx import Graph
from .game import Game
from .player import Cops, Robber

game = None

print("To play tictactoe as cops write \'C\', as robber \'R\' and for manual game write \'M\':")
while game is None:
    player = input()

    if not isinstance(player, str):
        print("Wrong input. Try again:")
        continue
    player = player.upper()

    if player == 'C':
        game = Game(None, Robber)
    elif player == 'R':
        game = Game(Cops, None)
    elif player == 'M':
        game = Game()
    elif player == 'B':
        game = Game(Cops, Robber)
    elif player == 'T':
        game = None
        break
    else:
        print("Wrong input. Try again:")

if player == 'B':
    game.run()
else:
    if player == "R":
        board = TicTacToeBoard(game, False)
    else:
        board = TicTacToeBoard(game)
    board.mainloop()"""

board = TicTacToeBoard()
board.mainloop()
