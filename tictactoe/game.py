import traceback
from .tictactoe_graph import Graph
from .generic_player import *
from .logging import getLogger, DEBUG

DEBUG = False

if DEBUG:
    game_logger = getLogger('game', console_level=DEBUG)
else:
    game_logger = getLogger('game')

history_logger = getLogger('history', mode='a')

class Game(object):
    def __init__(self, cops_engine: GenericEngine = None, robber_engine: GenericEngine = None, map = None) -> None:
        """Constructor for the Game class

        The Cops class is available via cops_engine.Cops
        and the Robber class is available via robber_engine.Robber.

        :param engine: engine used for the cops or robber 
        :param player: R for robber and C for cops
        """
        self.reset_game(map)

        if robber_engine is None and cops_engine is None:
            self.__init_cops(manual_cops)
            self.__init_robber(manual_robber)
            return
        elif robber_engine is not None and cops_engine is not None:
            self.__init_cops(cops_engine)
            self.__init_robber(robber_engine)
        elif cops_engine is not None:
            self.__init_robber(manual_robber)
            self.__init_cops(cops_engine)
        elif robber_engine is not None:
            self.__init_cops(manual_cops)
            self.__init_robber(robber_engine)

    def reset_game(self, map = None) -> None:
        """Initialises main components of the game."""
        self.graph = Graph(map)
        self.__result = 0
        self.round = self.graph.labels_count
        self.__status = 'Game continues'

    def __init_cops(self, cops_engine) -> None:
        """Initialises self.cops.
        :param cops_engine: the engine to use to initialise Cops
        """
        self.cops = cops_engine
        game_logger.debug(f"{self.cops} have been defined")

    def __init_robber(self, robber_engine) -> None:
        """Initialises self.robber.
        :param robber_engine: the engine to use to initialise Robber
        """
        self.robber = robber_engine
        game_logger.debug(f"{self.robber} has been defined")

    @property
    def winner_combo(self) -> list[int]:
        """Returns winning combination. If there are no winning combinations, returns []."""
        game_logger.debug("winner_combo:")
        if self.is_winner(self.cops):
            return self.__winner_combo(self.cops.label)
        elif self.is_winner(self.robber):
            return self.__winner_combo(self.robber.label)
        else:
            game_logger.debug("Nobody won")
            return []

    def __winner_combo(self, label: str) -> None:
        """Helper method for winner_combo."""
        for move in [(0, 0), (1, 1), (2, 2)]:
            for node in self.graph.neighbors(move):
                winning_comb = []
                num = 0
                for m in self.graph.neighbors(node):
                    if self.graph[m] == label:
                        num += 1
                        winning_comb.append(m)
                if num == 3:
                    game_logger.debug(f"winning combo is {winning_comb}")
                    return winning_comb
        game_logger.warning(f"Warning: no winning combo was found")
        return []

    def print(self) -> None:
        """Prints the game board in console and highlights the winning combination."""
        winning_combo = self.winner_combo
        history_logger.info('\n' + self.graph.print(winning_combo))

    @property
    def current_player(self) -> GenericPlayer:
        """Returns current player."""
        if self.round % 2 == 0:
            return self.robber
        else:
            return self.cops

    @property
    def next_player(self) -> GenericPlayer:
        """Returns next player."""
        if self.round % 2 == 0:
            return self.cops
        else:
            return self.robber

    def next_step(self) -> tuple[int, int]:
        """Computes a single round of the game.

        Cops have the first move, and then move alternating with the Robber.
        """
        self.round += 1
        history_logger.info(f"Round {self.round}")
        game_logger.debug(f"Current player: {self.current_player}")
        if self.current_player == self.cops:
            if self.cops.name == "engine_cops":
                step = self.cops_step()
            else:
                raise Exception("Cops engine is not defined")
        else:
            if self.robber.name == "engine_robber":
                step = self.robber_step()
            else:
                raise Exception("Robber engine is not defined")
        self.print()
        return step

    def next_manual_step(self, move: tuple[int, int]) -> None:
        """Proceeds a given manual move."""
        self.round += 1
        history_logger.info(f"Round {self.round}")
        game_logger.debug(f"Current player: {self.current_player}")
        if self.current_player == self.cops:
            self.__cop_manual_step(move)
        else:
            self.__robber_manual_step(move)
        self.print()

    def __cop_manual_step(self, cop_move: tuple[int, int]) -> None:
        """Proceeds a given cops manual move."""
        self.__set_cop_position(cop_move)
        if self.is_winner(self.cops):
            self.__cops_three_in_a_row()
        elif self.is_tied():
            self.__tie()

    def __robber_manual_step(self, robber_move: tuple[int, int]) -> None:
        """Proceeds a given robbers manual move."""
        self.__set_robber_position(robber_move)
        if self.is_winner(self.robber):
            self.__robber_three_in_a_row()
        elif self.is_tied():
            self.__tie()

    def robber_step(self) -> tuple[int, int]:
        """Computes a single robber step.

        In case of an exception, the exception is logged and the cops win.
        Otherwise, we add the new label given by robber's step method to the graph.
        """
        robber_move: tuple[int, int] | None = None
        try:
            robber_move = self.robber.step(self.graph)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__robber_exception()
        self.__set_robber_position(robber_move)
        if self.is_winner(self.robber):
            self.__robber_three_in_a_row()
        return robber_move

    def cops_step(self) -> tuple[int, int]:
        """Computes a single cops step.

        In case of an exception, the exception is logged and the robber wins.
        Otherwise, we add the new label given by cops' step method to the graph.
        """
        cop_move: tuple[int, int] | None = None
        try:
            cop_move = self.cops.step(self.graph)
        except Exception as e:
            game_logger.info(traceback.format_exception(type(e), e, e.__traceback__))
            self.__cops_exception()
        self.__set_cop_position(cop_move)
        if self.is_winner(self.cops):
            self.__cops_three_in_a_row()
        if self.is_tied():
            self.__tie()
        return cop_move

    @property
    def result(self) -> int:
        """Returns the current result of the game.

        :return: 0 if the game continues, -1 if the robber wins, 1 if the cops win and 2 if the game tied
        """
        return self.__result

    @property
    def status(self) -> str:
        """Returns the current status of the game.

        Possible values are
        - Game continues
        - Cops invalid step
        - Robber invalid step
        - Exception in cops call
        - Exception in robber call
        - Robber caught
        """
        return self.__status

    def is_valid_move(self, move) -> bool:
        """Checks if a given move is valid."""
        if move is None:
            return False
        if move[0] < 0 or move[1] < 0 or move[0] > 2 or move[1] > 2 or self.graph[move] != ' ':
            return False
        return True

    def __set_cop_position(self, move: tuple[int, int]) -> None:
        """Updates the graph with the cop label on node {move} if the given tuple is a valid move

        If the given tuple is None, the robber wins, i.e. __result is set to -1.
        If the given tuple is not a valid move, the robber wins.
        If the given tuple is already labelled, the robber wins.
        Otherwise, adds the next label in the free node.
        :param move: the node to be labelled next
        """
        history_logger.info("Cops move: " + str(move))
        if not self.is_valid_move(move):
            self.__robber_invalid_step()
            return
        self.graph[move] = self.cops.label

    def __set_robber_position(self, move: tuple[int, int]) -> None:
        """Updates the graph with the robber label on node {move} if the given tuple is a valid move

        If the given position is None, the cop wins, i.e. __result is set to 1.
        If the given position is not a valid position, the cops win.
        If the given position is already labelled, the cops win.
        Otherwise, adds the next label in the free node.
        :param move: the node to be labelled next
        """
        history_logger.info("Robber move: " + str(move))
        if not self.is_valid_move(move):
            self.__robber_invalid_step()
            return
        self.graph[move] = self.robber.label

    def is_winner(self, player: GenericPlayer) -> int:
        """Checks if the given player has a winning three labels in-a-row, i.e. a node such that all neighbors share the same label.

        :param player: the player used to play in the graph
        :return: 1 if there are currently three labels in-a-row, and 0 if there are not
        """
        for v in self.graph.nodes:
            win = True
            for w in self.graph.neighbors(v):
                if self.graph[w] != player.label:
                    win = False
            if win:
                game_logger.debug(f"{player} is winner")
                return 1
        return 0

    def has_winner(self) -> bool:
        """Checks if game has a winner."""
        if self.is_winner(self.robber) or self.is_winner(self.cops):
            return True
        else:
            return False

    def __cops_three_in_a_row(self) -> None:
        """If the cops have three-in-a-row, the cops win and the status of the game is set to 'Robber caught'."""
        self.__cops_win()
        self.__set_status('Robber caught')

    def is_tied(self) -> int:
        """Checks if the game is tied.

        :return: 1 if all (i,j) nodes have labels other than ' ', and 0 if at least one (i,j) node is still labelled with ' '.
        """
        for i in range(3):
            for j in range(3):
                if self.graph[(i,j)] == ' ':
                    return 0
        return 1

    def __tie(self) -> None:
        """If the game ends in a tie, the robber wins and the status of the game is set to 'Cops shift ends'."""
        history_logger.info("Tie. Cops shift ends")
        self.__set_status('Cops shift ends')
        self.__result = 2

    def __robber_three_in_a_row(self) -> None:
        """If the robber has three-in-a-row, the robber wins and the status of the game is set to 'Robber escapes'."""
        self.__robber_win()
        self.__set_status('Robber escapes')

    def __cops_invalid_step(self) -> None:
        """If the cops make an invalid step, robber wins and the status of the game is set to 'Cops invalid step.'"""
        self.__robber_win()
        self.__set_status('Cops invalid step')

    def __robber_invalid_step(self) -> None:
        """If robber makes an invalid step, the cops win and the status of the game is set to 'Robber invalid step.'"""
        self.__cops_win()
        self.__set_status('Robber invalid step')

    def __cops_exception(self) -> None:
        """If an exception is raised in one of the cop calls, robber wins and the status of the game is set to
        'Exception in cops call'.
        """
        self.__robber_win() 
        self.__set_status('Exception in cops call')

    def __robber_exception(self) -> None:
        """If an exception is raised in one of the robber calls, the cops win and the status of the game is set to
        'Exception in robber call'.
        """
        self.__cops_win()
        self.__set_status('Exception in robber call')

    def __robber_win(self) -> None:
        """Sets __result to -1 if the game still continues."""
        if self.__result == 0:
            self.__result = -1

    def __cops_win(self) -> None:
        """Sets __result to 1 if the game still continues."""
        if self.__result == 0:
            self.__result = 1

    def __set_status(self, status: str) -> None:
        """Sets __status to the given value if the game still continues."""
        if self.__status == 'Game continues':
            self.__status = status

    def run(self) -> None:
        """Runs the game bot vs bot and outputs in console."""
        if self.cops.name == "engine_cops" and self.robber.name == "engine_robber":
            result = 0
            while result == 0:
                self.next_step()
                result = self.result


def main():
    """Starts tictactoe bot vs bot."""
    from .engine import engine_cops, engine_robber
    game = Game(engine_cops, engine_robber)
    game.run()

if __name__ == "__main__":
    main()