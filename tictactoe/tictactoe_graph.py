from networkx import Graph as _nx_Graph

class Graph:
    def __init__(self, map = None):
        """Initialises self._graph.

        The tictactoe graph is a networkx.Graph.
        Nodes are either representing spaces (i,j), rows ('row',i), columns ('col',j), or diagonals ('dia',i).
        Spaces are connected to rows, columns, or diagonals by an edge with respect to their position on the 3x3 tic-tac-toe grid.
        Nodes also contain a label as an attribute, initialised with a space ' '.
        Label of spaces can be self.cops.label or self.robber.label; Label of rows, columns or diagonals have to be always empty.
        """
        self._graph = _nx_Graph()

        for i in range(2):
            self._graph.add_node(('dia', i), label=' ')

        for i in range(3):
            self._graph.add_node(('row', i), label=' ')
            for j in range(3):
                if i == 0:
                    self._graph.add_node(('col', j), label=' ')
                self._graph.add_node((i, j), label=' ')

                self._graph.add_edge((i, j), ('row', i))
                self._graph.add_edge((i, j), ('col', j))
                if i == j:
                    self._graph.add_edge((i, j), ('dia', 0))
                if i == 2 - j:
                    self._graph.add_edge((i, j), ('dia', 1))
        if map is not None:
            self.read(map)


    def __getitem__(self, move: tuple[int, int]) -> str:
        return self._graph.nodes[move]['label']

    def __setitem__ (self, move: tuple[int, int], label: str):
        self._graph.nodes[move]['label'] = label

    def neighbors(self, node) -> tuple[tuple[int, int]]:
        return self._graph.neighbors(node)

    @property
    def labels(self) -> list[list[str]]:
        """Example of labels: [['X', ' ', ' '], ['X', 'O', ' '], ['O', ' ', ' ']]"""
        game_nodes = list(filter(lambda x: isinstance(x[0][0], int), self._graph.nodes.data()))
        game_nodes = [x[1]['label'] for x in game_nodes]
        labels = [[game_nodes[0], game_nodes[1], game_nodes[2]],
                 [game_nodes[3], game_nodes[4], game_nodes[5]],
                 [game_nodes[6], game_nodes[7], game_nodes[8]]]
        return labels

    @property
    def labels_count(self) -> int:
        count = 0
        for x in self.labels:
            for y in x:
                if y != ' ':
                    count += 1
        return count

    @property
    def nodes(self) -> list[tuple[int, int]]:
        return self._graph.nodes

    @property
    def fields(self) -> list[tuple[int, int]]:
        return [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]

    def print(self, highlighted_spaces: list = []) -> str:
        """Returns the game board as string and highlights the given spaces with brackets []."""
        labels = self.labels

        for (i, j) in self.fields:
            if (i, j) in highlighted_spaces:
                labels[i][j] = "[" + str(labels[i][j]) + "]"
            else:
                labels[i][j] = " " + str(labels[i][j]) + " "

        map =  "┏━━━┯━━━┯━━━┓\n"
        map += "┃" + labels[0][0] + "│" + labels[0][1] + "│" + labels[0][2] + "┃\n"
        map += "┠───┼───┼───┨\n"
        map += "┃" + labels[1][0] + "│" + labels[1][1] + "│" + labels[1][2] + "┃\n"
        map += "┠───┼───┼───┨\n"
        map += "┃" + labels[2][0] + "│" + labels[2][1] + "│" + labels[2][2] + "┃\n"
        map += "┗━━━┷━━━┷━━━┛\n"
        return map

    def __str__(self):
        return self.print()

    def read(self, map: str) -> None:
        """ Reads the following image of a game board:
           ┏━━━┯━━━┯━━━┓
           ┃ X │ X │ O ┃
           ┠───┼───┼───┨
           ┃ O │ O │ X ┃
           ┠───┼───┼───┨
           ┃ X │ O │ X ┃
           ┗━━━┷━━━┷━━━┛
        """
        # to delete first empty line if it exists
        if map[0] == '\n':
            map = map[1:]
        map = map.split('\n')

        G = self._graph.nodes
        G[(0, 0)]["label"] = map[1][2]
        G[(0, 1)]["label"] = map[1][6]
        G[(0, 2)]["label"] = map[1][10]
        G[(1, 0)]["label"] = map[3][2]
        G[(1, 1)]["label"] = map[3][6]
        G[(1, 2)]["label"] = map[3][10]
        G[(2, 0)]["label"] = map[5][2]
        G[(2, 1)]["label"] = map[5][6]
        G[(2, 2)]["label"] = map[5][10]

    three_in_a_row = [[(0, 0), (0, 1), (0, 2)], [(1, 0), (1, 1), (1, 2)],
                [(2, 0), (2, 1), (2, 2)],  [(0, 2), (1, 1), (2, 0)],
                [(0, 0), (1, 1), (2, 2)]]

if __name__ == '__main__':
    graph = Graph()
    print(graph.nodes)