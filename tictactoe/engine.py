from .tictactoe_graph import Graph
from .generic_player import GenericEngine
from . import logging

Move = tuple[int, int]

first_corner_move = False
move_explanations = True
debug = False

if debug:
    logger = logging.getLogger('engine', console_level=logging.DEBUG)
elif move_explanations:
    logger = logging.getLogger('engine', console_level=logging.INFO)
else:
    logger = logging.getLogger('engine', console_level=logging.WARNING)

class Engine(GenericEngine):
    """Helper class for Cops and Robber"""
    def getMoves(self, graph: Graph) -> list[Move]:
        """Shows possible moves"""
        moves = []
        for i in range(3):  
            for j in range(3):
                if graph[(i,j)] == ' ':
                    moves.append((i,j))
        return moves

    def _isWinning(self, move: Move, graph: Graph) -> bool:
        """Checks if your move wins the game"""
        for node in graph.neighbors(move):
            num = 0
            for m in graph.neighbors(node):
                if graph[m] == self.label:
                    num +=1
            if num > 1:
                return True
        return False
    
    def _isOppWinning(self, move: Move, graph: Graph) -> bool:
        """Checks if opponents move wins the game"""
        for node in graph.neighbors(move):
            num = 0
            for m in graph.neighbors(node):
                if graph[m] == self.opp_label:
                    num +=1
            if num > 1:
                return True
        return False

    def _isFree(self, node, graph) -> bool:
        """ Checks if the row, column or diagonal free of opp_labels and contains at least one label

        :param node: the row, column or diagonal of graph
        """
        num = 0
        for n in graph.neighbors(node):
            if graph[n] == self.opp_label:
                return False
            if graph[n] == self.label:
                num += 1
        return num > 0

    def _isCoFree(self, node, graph: Graph) -> bool:
        """ Checks if the row, column or diagonal free of labels and contains at least one opp_label

        :param node: the row, column or diagonal of graph
        """
        num = 0
        for n in graph.neighbors(node):
            if graph[n] == self.label:
                return False
            if graph[n] == self.opp_label:
                num +=1
        return num > 0
        
    def _isFork(self, move: Move, graph: Graph) -> bool:
        """Checks if our move is fork"""
        num_free = 0
        for n in graph.neighbors(move):
            if self._isFree(n, graph):
                num_free += 1
        if num_free > 1:
            return True
        return False
    
    def _isOppFork(self, move: Move, graph: Graph) -> bool:
        """Checks if opponents move is fork"""
        num_free = 0
        for n in graph.neighbors(move):
            if self._isCoFree(n, graph):
                num_free += 1
        if num_free > 1:
            return True
        return False

    def _third_in_a_row(self, graph: Graph) -> bool:
        """First checks if the game has two labels in-a-row. Then returns (if it exists) the third missing space.

        :return: None if there are no two labels in-a-row with third empty space, else return this emtpy space.
        """
        logger.debug("- Check if the game has two labels in-a-row:")
        for three_in_a_row in graph.three_in_a_row:
            labels = [graph[x] for x in three_in_a_row]
            logger.debug(f"-- three in a row {three_in_a_row} with {labels}")
            if ' ' in labels and labels.count(self.label) == 2:
                for x in three_in_a_row:
                    if graph[x] == ' ':
                        logger.debug(f"--- Found third in a row: {x}.")
                        return x
        return None

    def _prevents_multiple_fork(self, move: Move, graph: Graph) -> bool:
        """Checks if the given move prevents an opponents fork."""
        logger.debug(f"Assume that graph[{move}] = {self.label}:")
        graph[move] = self.label
        third_in_a_row = self._third_in_a_row(graph)
        # if third in a row does not produce another fork:
        if third_in_a_row is not None and not self._isOppFork(third_in_a_row, graph):
            res = True
            logger.debug(f"--- Found third in a row {third_in_a_row} does not cause an opponent fork.")
        else:
            res = False
            if third_in_a_row is not None and self._isOppFork(third_in_a_row, graph):
                logger.debug(f"--- Found third in a row {third_in_a_row} causes an opponent fork.")
        graph[move] = ' '
        return res
    
    def solver(self, graph: Graph) -> Move:
        """Perfect play  of tic-tac-toe: used in Newell and Simon's 1972 tic-tac-toe program.

        :param return: [] if there are no moves available
        """

        moves = self.getMoves(graph)
        if self.getMoves(graph) == []:
            return []

        # 0. playing the corner gives the opponent the smallest choice of squares which must be played to avoid losing
        if first_corner_move:
            if graph.labels_count == 0:
                logger.info("Corner moves gives the opponent the smallest choice of squares which must be played to avoid losing.")
                return (0, 0)

        # 1. Win: If the player has two in a row, they can place a third to get three in a row.
        for move in moves:
            # Does the move win the game?"
            if self._isWinning(move, graph):
                logger.info(f"Move {move} wins game.")
                return move

        # 2. Block: If the opponent has two in a row, the player must play the third themselves to block the opponent.
        for move in moves:
            # does the move win the opponent's game?
            if self._isOppWinning(move, graph):
                logger.info(f"Move {move} would win opp game.")
                return move

        # 3. Fork: Cause a scenario where the player has two ways to win (two non-blocked lines of 2).
        for move in moves:
            # would this move enable us to fork the opponent?
            if self._isFork(move, graph):
                logger.info(f"Move {move} forks opp.")
                return move

        # 4. Blocking an opponent's fork:
        fork_moves = []
        for move in moves:
            # would this move enable the opponent to fork us?
            if self._isOppFork(move, graph):
                fork_moves.append(move)

        if len(fork_moves) == 1:
            logger.info(f"Move {fork_moves[0]} prevents opp fork.")
            return fork_moves[0]

        elif len(fork_moves) > 1:
            # if there are multiple possibilities for opp to fork us, we should make a two in a row to force the opponent into defending
            logger.info("There are multiple fork moves: " + str(fork_moves))
            # first try to block all forks simultaneously
            for move in fork_moves:
                if self._prevents_multiple_fork(move, graph):
                    logger.info(f"Move {move} blocks multiple opp fork.")
                    return move
            # if it is not possible then just force opponent into defending
            for move in set(moves) - set(fork_moves):
                if self._prevents_multiple_fork(move, graph):
                    logger.info(f"Move {move} causes two-in-a-row to force the opponent into defending.")
                    return move

        # 5. Central space must be occupied:
        if (1, 1) in moves:
            if graph.labels_count == 0:
                logger.info(f"If the players are not perfect, an opening move (1, 1) in the center is best for {self.label}.")
            else:
                logger.info("Move (1, 1) occupies central space.")
            return (1, 1)

        # 6. Opposite corner: If the opponent is in the corner, the player plays the opposite corner.
        corner_moves = [(0, 0), (0, 2), (2, 0), (2, 2)]
        for move in corner_moves:
            opp_move = (abs(move[0] - 2), abs(move[1] - 2))
            if graph[move] == self.opp_label and graph[opp_move] == ' ':
                logger.info("Move occupies opposite corner: " + str(opp_move))
                return opp_move

        # 7. Empty corner: The player plays in a corner square.
        emtpy_corners = list(set(moves) & set(corner_moves))
        if emtpy_corners == []:
            logger.info("All corners are occupied")
        else:
            logger.info(f"Move {emtpy_corners[0]} occupies empty corner.")
            return emtpy_corners[0]

        # 8. Empty side: The player plays in a middle square on any of the four sides.
        side_moves = [(0, 1), (2, 1), (1, 0), (1, 2)]
        emtpy_side_moves = list(set(moves) & set(side_moves))
        if emtpy_side_moves != []:
            logger.info(f"Move {emtpy_side_moves[0]} occupies empty side.")
            return emtpy_side_moves[0]
        else:
            logger.info("All side moves are occupied")
            print("Warning: you can't be here")
            return moves[0]

    def bruteforce(self, graph, label, count) -> tuple[Move, int, int]:  # = (next_move, depth, result)
        raise NotImplementedError
    """def bruteforce(self, graph, label, count) -> tuple[Move, int]:
        #Old recursion for bruteforce method.

        #:graph: have not to be full else exception
        #:param count: the depth of recursion
        #:param return: the best, shortest move and his depth
        
        count += 1
        moves = self.getMoves(graph)
        if self.getMoves(graph) == []:
            return Exception
        min = 9
        logger.info("Depth is " + str(count))
        # bruteforce:
        # stop condition: last possible move
        if len(moves) == 1:
            logger.info("Depth is maximal")
            return (moves[0], count)

        # choose the optimal (with minimal depth) step
        next_move = None
        for move in moves:
            logger.info(f"Next move as {label}: {move}")

            if(label == self.label):
                graph[move] = self.label
                m = self.bruteforce(graph, self.opp_label, count)[1]
            else:
                graph[move] = self.opp_label
                m = self.bruteforce(graph, self.label, count)[1]
            graph[move] = ' '
            # choose the minimal move
            if min > m:
                min = m
                next_move = move

        if next_move is None:
            raise Exception
        else:
            logger.info(f"Final move as {label}: {next_move}")
            return (next_move, min)"""

    def step(self, graph: Graph) -> Move:
        """Computes the node to be labelled with the next player based on the current position.

        :return: the next player move
        """
        #next_move = self.bruteforce(graph, self.label, 0)[0]
        #logger.info(f"Bruteforce is succeed.")
        #return next_move
        return self.solver(graph)

engine_cops = Engine(name="engine_cops", label="X", opp_label="O", color="blue")
engine_robber = Engine(name="engine_robber", label="O", opp_label="X", color="green")