from unittest import TestCase, main
from tictactoe.game import Game
from ..engine import engine_cops, engine_robber

def CheckNodesEdges():
    """Just to check the game structure"""
    print("Alle Nodes:")
    print(game.graph.nodes)

    print("Alle Edges:")
    print(game.graph.edges)

    print("Benachbarte Knoten:")
    for v in game.graph.nodes:
        for w in game.graph.adj[v]:
            print(v, "-", w, game.graph.nodes[w]['label'])

def Example1():
    """1. Example from issue"""
    map = """
┏━━━┯━━━┯━━━┓
┃ X │   │   ┃
┠───┼───┼───┨
┃   │ O │   ┃
┠───┼───┼───┨
┃   │   │ X ┃
┗━━━┷━━━┷━━━┛"""
    """ False move:
┏━━━┯━━━┯━━━┓
┃ X │   │   ┃
┠───┼───┼───┨
┃   │ O │   ┃
┠───┼───┼───┨
┃(O)│   │ X ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    return game

def Example2():
    """2. Example from issue"""
    map = """
┏━━━┯━━━┯━━━┓
┃ X │   │   ┃
┠───┼───┼───┨
┃ O │ X │   ┃
┠───┼───┼───┨
┃   │   │   ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    return game

def Example3():
    """3. Example from issue"""
    map = """
┏━━━┯━━━┯━━━┓
┃ X │   │   ┃
┠───┼───┼───┨
┃ X │ O │   ┃
┠───┼───┼───┨
┃ O │   │   ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    """
┏━━━┯━━━┯━━━┓
┃ X │   │(X)┃ - right move
┠───┼───┼───┨
┃ X │ O │   ┃
┠───┼───┼───┨
┃ O │(X)│   ┃ - wrong move
┗━━━┷━━━┷━━━┛"""
    return game


def Example4():
    """Almost full game board"""
    map = """
┏━━━┯━━━┯━━━┓
┃ X │ O │ X ┃
┠───┼───┼───┨
┃ X │ O │ O ┃
┠───┼───┼───┨
┃ O │ X │   ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    return game

def Example5():
    """Robber did a wrong move"""
    map = """
┏━━━┯━━━┯━━━┓
┃ O │   │   ┃
┠───┼───┼───┨
┃   │ X │ X ┃
┠───┼───┼───┨
┃   │   │   ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    """
┏━━━┯━━━┯━━━┓
┃ O │   │(O)┃ - wrong move
┠───┼───┼───┨
┃(O)│ X │ X ┃ - right move
┠───┼───┼───┨
┃   │   │   ┃
┗━━━┷━━━┷━━━┛"""
    return game

def Example6():
    """Cops did unnecessary move"""
    map = """
┏━━━┯━━━┯━━━┓
┃ X │ X │ O ┃
┠───┼───┼───┨
┃ O │ X │   ┃
┠───┼───┼───┨
┃   │   │ O ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    """
┏━━━┯━━━┯━━━┓
┃ X │ X │ O ┃
┠───┼───┼───┨
┃ O │ X │(X)┃ - wrong move
┠───┼───┼───┨
┃   │(X)│ O ┃ - right move
┗━━━┷━━━┷━━━┛"""
    return game

def prevent_mutliple_fork():
    map = """
┏━━━┯━━━┯━━━┓
┃ R │   │   ┃
┠───┼───┼───┨
┃   │ C │   ┃
┠───┼───┼───┨
┃   │   │ C ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    return game


def set_fork():
    map = """
┏━━━┯━━━┯━━━┓
┃ X │ O │   ┃
┠───┼───┼───┨
┃   │ X │   ┃
┠───┼───┼───┨
┃   │   │ O ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    return game

def prevemt_mult_fork():
    map = """
┏━━━┯━━━┯━━━┓
┃ X │   │   ┃
┠───┼───┼───┨
┃   │ O │   ┃
┠───┼───┼───┨
┃   │   │ X ┃
┗━━━┷━━━┷━━━┛"""
    game = Game(engine_cops, engine_robber, map)
    return game

class PlayerTest(TestCase):         # testing kit
    def setUp(self):
        print("\n" + self._testMethodName + ":")
    @classmethod
    def setUpClass(self):
        self.G1 = Example1()
        self.G2 = Example2()
        self.G3 = Example3()
        self.G4 = Example4()
        self.G5 = Example5()
        self.G6 = Example6()

    def test_getMoves1(self):
        moves1 = [(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1)]
        self.assertEqual(engine_cops.getMoves(self.G1.graph), moves1)

    def test_getMoves2(self):
        moves3 = [(2, 2)]
        self.assertEqual(engine_cops.getMoves(self.G4.graph), moves3)
    
    def test__isOppFork1(self):    # Fork
        self.assertTrue(engine_cops._isFork((0, 1), self.G2.graph))

    #  from here
    def test__isOppWinning1(self):    # otherwise three in a row
        self.assertTrue(engine_robber._isOppWinning((2, 2), self.G2.graph))
        self.assertFalse(engine_cops._isOppWinning((2, 2), self.G2.graph))

    def test__isOppFork2(self):    # otherwise OppFork
        self.assertTrue(engine_cops._isOppFork((2, 1), self.G3.graph))

    def test__isOppWinning2(self):    # otherwise three in a row
        self.assertTrue(engine_cops._isOppWinning((0, 2), self.G3.graph))

    def test_full_game1(self):
        self.G3.run()
        self.assertEqual(self.G3.result, 2)

    def test_full_game2(self):
        game = Game(engine_cops, engine_robber)
        game.graph.nodes[(0, 0)]["label"] = "X"
        game.round = 1
        game.run()
        self.assertEqual(game.result, 2)

    def test_full_game3(self):
        self.G6.run()
        self.assertEqual(self.G6.result, 1)

    def test_example5(self):
        self.assertEqual(engine_robber.step(self.G5.graph), (1, 0))

    def test_example6(self):
        self.assertEqual(engine_cops.step(self.G6.graph), (2, 1))

    def test_prevent_mutliple_fork(self):
        game = prevent_mutliple_fork()
        # prevents multiple fork
        self.assertIn(engine_robber.step(game.graph), [(0, 2), (2, 0)])

    def test_set_fork(self):
        game = set_fork()
        # two forks are available
        self.assertIn(engine_cops.step(game.graph), [(1, 0), (2, 0)])

    def test_prevemt_mult_fork(self):
        game = prevemt_mult_fork()
        self.assertIn(engine_robber.step(game.graph), [(1, 0), (0, 1), (2, 1), (1, 2)])


if __name__ == '__main__':
    main()


