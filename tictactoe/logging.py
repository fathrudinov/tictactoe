import logging as _logging
from logging import getLogger as _getLogger
import sys
# to restrict file output to a certain size
from logging.handlers import RotatingFileHandler as _RotatingFileHandler

debug = False
DEBUG = _logging.DEBUG
INFO = _logging.INFO
WARNING = _logging.WARNING

# list of existing loggers
logger_list = []

class getLogger:
    """This is our modified logger configuration."""
    def __init__(self, name, mode = 'w', file_level = _logging.DEBUG, console_level = _logging.INFO, time_output = True):
        """Set up file output and console output."""
        global debug
        if debug:
            print(f"creating new logger \'{name}\'")
        file_name = name + '.log'

        if name not in logger_list:
            logger_list.append(name)
        else:
            # to not overwrite already existing logger with different configurations
            self.logger = _getLogger(name)
            self.file_name = file_name
            return

        logger = _getLogger(name)
        logger.setLevel(_logging.DEBUG)  # set root's level

        #file_handler = _RotatingFileHandler(log_name, mode=mode, maxBytes=1024)
        file_handler = _RotatingFileHandler(file_name, mode=mode, encoding='utf8')
        file_handler.setLevel(file_level)

        console_handler = _logging.StreamHandler(sys.stdout)
        console_handler.setLevel(console_level)

        if time_output:
            formatter = _logging.Formatter("%(asctime)s: %(message)s", "%Y-%m-%d %H:%M:%S")
            file_handler.setFormatter(formatter)

        logger.addHandler(file_handler)
        logger.addHandler(console_handler)

        self.logger = logger
        self.file_name = file_name

    def print(self, str):
        """Prints string in logger file without using handlers."""
        with open(self.file_name, 'a') as f:
            print(str, file=f)

    def clear(self):
        """Cleans the logger file."""
        open(self.file_name, 'w').close()

    def debug(self, *str):
        self.logger.debug(*str)

    def info(self, *str):
        self.logger.info(*str)

    def warning(self, *str):
        self.logger.warning(*str)

    def exception(self, *str):
        self.logger.exception(*str)

    def error(self, *str):
        self.logger.error(*str)