from typing import Type
from .tictactoe_graph import Graph
from abc import ABC, abstractmethod

# from typing import NamedTuple
# class GenericPlayer(NamedTuple):
class GenericPlayer:
    """Structure for player"""
    name: str
    label: str
    opp_label: str
    color: str
    def __init__(self, name, label, opp_label, color):
        self.name = name
        self.label = label
        self.opp_label = opp_label
        self.color = color
    def __str__(self):
        return self.name

manual_cops = GenericPlayer(name="manual_cops", label="X", opp_label= "O", color="blue")
manual_robber = GenericPlayer(name="manual_robber", label="O", opp_label="X", color="green")

DEFAULT_PLAYERS = (manual_cops, manual_robber)

class GenericEngine(ABC, GenericPlayer):
    """Abstract parent class for Cops and Robber"""
    @abstractmethod
    def step(self, graph: Graph) -> tuple[int, int]:
        """Computes next game position.

        This property's setter is marked as abstract and to be overridden in
        inheriting classes based on the rules of the implemented game.

        :raises NotImplementedError: This property's setter raises a
        'NotImplementedError' unless it is overridden.
        """
        raise NotImplementedError