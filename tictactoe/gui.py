"""A tic-tac-toe game built with Python and Tkinter."""

import tkinter as tk
#import tk.ttk as ttk
from tkinter import font

from .game import Game, DEFAULT_PLAYERS
# logger is already defined in game.py
from .game import game_logger as logger
from .engine import engine_cops, engine_robber
from .logging import getLogger

#TODO: insert BOARD_SIZE constant in game.py
BOARD_SIZE = 3

class TicTacToeBoard(tk.Tk):
    # TODO: list here all attributes
    is_menu_created = False
    def __init__(self, game: Game = None, is_first_move: bool = True):
        super().__init__()
        self.configure(bg='white')
        self.minsize(500, 500)
        #self.geometry("500x500")
        self.title("Tic-Tac-Toe Game")
        self._cells = {}
        self._create_board_display()
        self._create_board_grid()
        if game is None:
            self._choose_game()
        else:
            self._game = game
            self._start(is_first_move)

    def _start(self, is_first_move = True):
        self._create_board()
        self._create_menu()

        if not is_first_move:
            self.__bot_step()
            self._update_display()

    def _create_menu(self):
        """Adds quit/reset buttons"""
        if not self.is_menu_created:
            buttons_frame = tk.Frame(master=self, bg='white')
            buttons_frame.pack()

            # add reset button
            self.reset_button = tk.Button(master=buttons_frame, text="Play again", command=self.reset_board, background='light green',
                               font='Helvetica 16 bold', width=10)
            self.reset_button.grid(row=0, column=0)
            self.reset_button["state"] = "disabled"

            # add quit button:
            quit_button = tk.Button(master=buttons_frame, text="Quit", command=quit, background='light green',
                               font='Helvetica 16 bold', width=10)
            quit_button.grid(row=0, column=1)
        self.is_menu_created = True

    def _create_board_display(self):
        display_frame = tk.Frame(master=self, bg='white')
        display_frame.pack(fill=tk.X)
        self.display = tk.Label(
            master=display_frame,
            text="Ready?",
            font=font.Font(size=28, weight="bold"),
            bg='white',
        )
        self.display.pack()

    def _create_board_grid(self):
        self.grid_frame = tk.Frame(master=self, bg='white')
        self.grid_frame.pack()

        for row in range(BOARD_SIZE):
            self.grid_frame.rowconfigure(row, weight=1)#, minsize=50)
            self.grid_frame.columnconfigure(row, weight=1)#, minsize=50)

        self.button_frame = list()

        for row in range(BOARD_SIZE):
            self.button_frame.append(list())
            for col in range(BOARD_SIZE):
                button_frame = tk.Frame(self.grid_frame, width=125, height=125, bg="lightblue")
                button_frame.grid_propagate(False) #disables resizing of frame
                button_frame.grid(row=row, column=col, sticky = "nsew", padx = 2, pady = 2)
                self.button_frame[row].append(button_frame)

    def _create_board(self):
        for row in range(BOARD_SIZE):
            for col in range(BOARD_SIZE):
                button = tk.Button(
                    master=self.button_frame[row][col],
                    text="",
                    font=font.Font(size=36, weight="bold"),
                    fg="black",
                    bg="lightblue",
                    #width=2,
                    #height=2,
                    highlightbackground="lightblue",
                )
                self._cells[button] = (row, col)
                button.bind("<ButtonPress-1>", self.play)
                button.place(x = 0, y = 0, width = 125, height = 125)

    def _choose_game(self):
        """Asks user for the player and then defines _game"""
        self.display.config(text="Choose player:")
        button1 = tk.Button(master=self.button_frame[1][0], text=DEFAULT_PLAYERS[0].label, font=font.Font(size=36, weight="bold"), bg='lightblue')
        button1.place(x = 0, y = 0, width = 125, height = 125)
        button1.bind("<ButtonPress-1>", self.__choose_game)
        self.button1 = button1

        button2 = tk.Button(master=self.button_frame[1][2], text=DEFAULT_PLAYERS[1].label, font=font.Font(size=36, weight="bold"), bg='lightblue')
        button2.place(x = 0, y = 0, width = 125, height = 125)
        button2.bind("<ButtonPress-1>", self.__choose_game)
        self.button2 = button2

        button3 = tk.Button(master=self.button_frame[1][1], text='both', font=font.Font(size=36, weight="bold"),bg='lightblue')#, bg='#0091ff')
        button3.place(x = 0, y = 0, width = 125, height = 125)
        button3.bind("<ButtonPress-1>", self.__choose_game)
        self.button3 = button3

    def __choose_game(self, event):
        """Button click function"""
        text = event.widget['text']
        if text == DEFAULT_PLAYERS[0].label:
            self._game = Game(None, engine_robber)
            is_first_move = True
        elif text == DEFAULT_PLAYERS[1].label:
            self._game = Game(engine_cops, None)
            is_first_move = False
        else:
            self._game = Game()
            is_first_move = True
        self.button1.place_forget()
        self.button2.place_forget()
        self.button3.place_forget()
        self._start(is_first_move)

    def play(self, event):
        """Handle a player's move."""
        clicked_btn = event.widget
        move = self._cells[clicked_btn]
        logger.debug(f"The button {move} was clicked")

        if self._game.is_valid_move(move) and self._game.result == 0:
            # proceed manual move
            self.__manual_step(clicked_btn)
            self._update_display()

            if self._game.result == 0 and not self._game.is_tied():
                # compute next engine move:
                if self._game.next_player.name in ["engine_cops", "engine_robber"]:
                    self.__bot_step()
                    self._update_display()

    def __manual_step(self, clicked_btn):
        move = self._cells[clicked_btn]
        logger.debug(f"Next manual move:")
        self._game.next_manual_step(move)
        self._update_button(clicked_btn)

    def __bot_step(self):
        logger.debug("Next engine move:")
        move = self._game.next_step()
        # self._cells is bijective, therefore find button from move:
        btn = None
        for key, value in self._cells.items():
            if value == move:
                btn = key
        if btn is None:
            raise Exception
        else:
            self._update_button(btn)

    def _update_button(self, clicked_btn):
        #clicked_btn["state"] = "disabled"
        #clicked_btn.unbind("<ButtonPress-1>")
        clicked_btn.config(text=self._game.current_player.label)
        clicked_btn.config(fg=self._game.current_player.color)

    def _highlight_cells(self):
        winning_combo = self._game.winner_combo
        for button, coordinates in self._cells.items():
            if coordinates in winning_combo:
                button.config(highlightthickness=3, highlightbackground='red', default="active", highlightcolor='red')

    def __update_display(self, msg, color="black"):
        self.display["text"] = msg
        self.display["fg"] = color

    def _update_display(self):
        if self._game.has_winner():
            logger.debug("Game has winner.")
            self._highlight_cells()
            msg = f'Player "{self._game.current_player.label}" won!'
            color = self._game.current_player.color
            self.__update_display(msg, color)
            self.reset_button["state"] = "normal"
        elif self._game.is_tied():
            logger.debug("Game is tied.")
            self.__update_display(msg="Tied game!", color="red")
            self.reset_button["state"] = "normal"
        else:
            msg = f"{self._game.next_player.label}'s turn"
            self.__update_display(msg)

    def reset_board(self):
        """Reset the game's board to play again."""
        self._game.reset_game()
        self.__update_display(msg="Ready?")

        for button in self._cells.keys():
            button.config(highlightbackground="lightblue")
            button.config(text="")
            button.config(fg="black")

        self.reset_button["state"] = "disabled"

        for btn in self._cells.keys():
            btn.place_forget()
        self._cells = {}

        self._choose_game()

def main():
    """Create the game's board and run its main loop."""
    game = Game(engine_cops, engine_robber)
    board = TicTacToeBoard(game)
    board.mainloop()

if __name__ == "__main__":
    main()
